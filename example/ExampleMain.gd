const ResponsiveHelper = preload('res://ResponsiveHelper.gd')

func _ready():
    # Example usage of responsive helper
    var responsive = ResponsiveHelper.new(1600, 900)

    # Background gets resized based on region rect
    responsive.add_rule(get_node('Background'), {maximize_region_rect=true})

    # Rat stays in the center
    responsive.add_rule(get_node('Rat'), {
        never_resize=true,
        min_spacing=Vector2(156, 200),
        center='horizontal',
        top=100,
    })

    # MaximizeRat always fills entire window
    responsive.add_rule(get_node('MaximizeRat'), {maximize=Vector2(156, 200)})

    # Add it to the parent node, and trigger a resize
    responsive.set_parent_node(self)


