extends Node

signal resized

var native_width
var native_height
var native_size

var root_node = null
var camera_node = null
var scene

# some useful public properties
var root_node_scale = null
var root_node_offset = null

var rules = []

var use_process = false

func _init(width, height):
    self.native_width = width
    self.native_height = height
    self.native_size = Vector2(width, height)

func set_camera(camera_node):
    self.camera_node = camera_node

func set_parent_node(node):
    self.root_node = node
    node.add_child(self)

    # If using "process" method, then use that, otherwise attach resize event
    if use_process:
        set_process(true)
    else:
        get_tree().connect("screen_resized", self, "do_resize")

    # Do initial resize:
    do_resize()

var _last_rect = Vector2(0, 0)
func _process(delta):
    var rect = get_viewport().get_rect().size
    if _last_rect == rect:
        return
    _last_rect = rect
    do_resize()

func add_rule(node, flags):
    rules.append([node, flags])

func get_scale(rect_size, native_size):
    var proportion = rect_size / native_size
    var scale
    if proportion.x > proportion.y:
        # Add left/right margin
        scale = Vector2(proportion.y, proportion.y)
    else:
        # Add top/bottom margin
        scale = Vector2(proportion.x, proportion.x)
    return scale

func resize_root_node(rect_size):
    var proportion = rect_size / native_size

    if proportion.x > proportion.y:
        # Add left/right margin
        root_node_scale = Vector2(proportion.y, proportion.y)
        root_node_offset = Vector2((proportion.x - proportion.y) / 2 * native_size.x, 0)
    else:
        # Add top/bottom margin
        root_node_scale = Vector2(proportion.x, proportion.x)
        root_node_offset = Vector2(0, (proportion.y - proportion.x) / 2 * native_size.y)

    if camera_node == null:
        # No camera, resize root node to achieve zoom effect
        root_node.set_pos(root_node_offset)
        root_node.set_scale(root_node_scale)
    else:
        # Camera was specified, instead adjust zoom and position of camera
        var inverted_scale = Vector2(1, 1) / root_node_scale
        camera_node.set_zoom(inverted_scale)
        camera_node.set_pos(root_node_offset * -inverted_scale)

func apply_rules(rect_size):
    var inverted_scale = Vector2(1, 1) / root_node_scale

    # Now we loop through the node rules and apply them
    for rule in rules:
        var node = rule[0]
        var flags = rule[1]
        var pos = node.get_pos()

        # Maximize region rect is for repeating backgrounds (texture with
        # "Repeat" set to true), and it will ensure the background fills the
        # entire frame no matter the dimensions
        if flags.has('maximize_region_rect') and flags.maximize_region_rect:
            node.set_region_rect(Rect2(Vector2(0, 0), rect_size / root_node_scale))
            pos = Vector2(-1, -1) * root_node_offset / root_node_scale

        # Make something maintain size as much as possible
        if flags.has('never_resize') and flags.never_resize:
            var scale = inverted_scale
            if flags.has('min_spacing'):
                var min_spacing = flags.min_spacing
                var overflow = min_spacing - rect_size
                if overflow.x > 0 or overflow.y > 0:
                    scale *= get_scale(rect_size, min_spacing)
            node.set_scale(scale)

        # Make something maintain size as much as possible
        if flags.has('maximize'):
            pos = Vector2(-1, -1) * root_node_offset / root_node_scale
            var scale = (rect_size / flags.maximize) * inverted_scale
            node.set_scale(scale)

        # Maximize only width
        if flags.has('maximize_width'):
            pos = Vector2(-1, -1) * root_node_offset / root_node_scale
            var scale = (rect_size / flags.maximize_width) * inverted_scale
            scale.y = node.get_scale().y # same as above just invert only width
            node.set_scale(scale)

        # Left and right
        if flags.has('left'):
            var offset = root_node_offset - Vector2(flags.left, 0)
            pos.x = -(offset * inverted_scale).x
        elif flags.has('right'):
            var offset = root_node_offset - Vector2(flags.right, 0)
            pos.x = (native_size + (offset * inverted_scale)).x

        # Top and bottom
        if flags.has('top'):
            var offset = root_node_offset - Vector2(0, flags.top)
            pos.y = -(offset * inverted_scale).y
        elif flags.has('bottom'):
            var offset = root_node_offset - Vector2(0, flags.bottom)
            pos.y = (native_size + (offset * inverted_scale)).y

        # Centering
        if flags.has('center'):
            var center_pos = native_size / Vector2(2, 2)
            if flags.center == 'both' or flags.center == 'horizontal':
                pos.x = center_pos.x
            if flags.center == 'both' or flags.center == 'vertical':
                pos.y = center_pos.y
        node.set_pos(pos)

func do_resize():
    # Handles root scale
    var rect = get_viewport().get_rect()
    resize_root_node(rect.size)
    apply_rules(rect.size)
    emit_signal('resized')

