# godot-responsive

Helper for making [Godot](http://godotengine.org/) scenes responsive, e.g.
adjust the display of 2D games based on different resolutions / dimensions.

Godot Engine is an awesome free software game engine that you should check out!

## Rational

Godot allows fixed ratio scaling that works pretty well, but the [16:9 ratio
only applies to ~66%](https://hwstats.unity3d.com/mobile/display.html) of
mobile gamers, and [60% of desktop
gamers](https://hwstats.unity3d.com/pc/display.html). Designing 2D games to
work with different resolutions and dimensions ("responsively") should be
easier. This package provides a helper that should make this easier.


## Usage

First make sure to unset `stretch_mode` and `stretch_aspect` in your
Project Settings (e.g.engine.cfg).

The main class is `ResponsiveHelper.gd`.  Example usage, assuming that you have
installed this module into a "submodules" directory in your game.


### Simple default behavior

This will resize the viewport while maintaining the aspect ratio, and keeping
everything vertically or horizontally centered. This is kind of like
`stretch_aspect = keep`, but it keeps the "margins" as being actually in the
scene.

This is often enough: A common usage (see Example) is just to make the BG a bit
bigger than the default to accommodate for different screen dimensions, and
your game may work fine across all resolutions.

```
const ResponsiveHelper = preload('res://submodules/godot-responsive/ResponsiveHelper.gd')

# Initialize with "default" resolution (should match project resolution).
var responsive = ResponsiveHelper.new(1980, 1024)

# Set the primary node of the current scene. Should be something that inherits
# from Node2D. ResponsiveHelper will update the scale and pos of this node.
# Must be done with every scene change that you want to be responsive.
responsive.set_parent_node(self)
```

### Custom rules

After you create create a helper you can add custom rules for certain nodes in
your scene, as such:

```
responsive.add_rule(get_node('Logo'), {never_resize=true, center=true})
```

All the optional rules are described below:

* `left`, `right`, `top`, `bottom` -- can anchor node to sides of screen

* `center` -- set to `both` to center on screen, or `horizontal` or `vertical`
  to only center one attribute

* `never_resize`-- this will attempt keep this node the same apparent size at
  all resolutions.

* `min_spacing` -- a `Vector2` that can be combined with `never_resize` to
  specify max top/bottom and left/right dimensions, which will be maintained.
  This will cause it to possibly shrink the node for smaller resolutions.  If
  the resolution is smaller than the obj, then it will shrink, otherwise it
  will attempt to rescale the node to its 'native' size (default behavior of
  `never_resize`).

* `maximize` -- a `Vector2` that should specify dimensions of this node, which
  will be maximized across entire screen (causing stretching).

* `maximize_region_rect`-- this will use `set_region_rect` and `set_pos` to
  make this item "fill" the screen entirely. This is useful for backgrounds
  that use a region rect, such as repeating backgrounds.

### Example

Check out the `examples` dir for an example, or just run this project in Godot.

